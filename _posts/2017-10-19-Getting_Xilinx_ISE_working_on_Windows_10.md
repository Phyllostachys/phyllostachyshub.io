---
layout: post
title:  "Getting Xilinx ISE working on Windows 10"
date:   2017-10-19
---

Xilinx decided to move to 'maintenance only' a tool that is exclusvely used to work on older Spartan FPGAs and Coolermaster CPLDs. This means that they don't support it on anything after Windows 7.

After some digging, though, I found [this](http://www.eevblog.com/forum/microcontrollers/guide-getting-xilinx-ise-to-work-with-windows-8-64-bit/). I'll copy it here in case that link is broken in the future.

> __Fixing Project Navigator, iMPACT and License Manager__  
> Note: I am assuming you are using ISE 14.7 and have installed it to the default location  
> 1. Open the following directory: C:\Xilinx\14.7\ISE_DS\ISE\lib\nt64
> 2. Find and rename libPortability.dll to libPortability.dll.orig
> 3. Make a copy of libPortabilityNOSH.dll (copy and paste it to the same directory) and rename it libPortability.dll
> 4. Copy libPortabilityNOSH.dll again, but this time navigate to C:\Xilinx\14.7\ISE_DS\common\lib\nt64 and paste it there
> 5. In C:\Xilinx\14.7\ISE_DS\common\lib\nt64 Find and rename libPortability.dll to libPortability.dll.orig
> 6. Rename libPortabilityNOSH.dll to libPortability.dll
> 
> __Fixing PlanAhead not opening from 64-bit Project Navigator__  
> PlanAhead will not open when you are running 64-bit Project Navigator (e.g. for I/O Pin Planning), it just displays the splash screen but never opens.  
> To fix it, we have to force PlanAhead to always run in 32-bit mode.  
> 1. Open C:\Xilinx\14.7\ISE_DS\PlanAhead\bin and rename rdiArgs.bat to rdiArgs.bat.orig
> 2. Download the attached zip file
> 3. Extract it. You should now have a file named rdiArgs.bat
> 4. Copy the new rdiArgs.bat file to C:\Xilinx\14.7\ISE_DS\PlanAhead\bin

That resolved the issues of actually getting ISE functional. Now, if you use one of Xilinx's Platform Cable I-IV USB, then you may run into another issue. If you use another programming pod that uses Jungo Connectivity's WinDriver USB, then you'll run into an issue where Xilinx thinks the new driver is unlicensed and the pod won't work.

To fix:
1. Uninstall or disable the newer driver in Device Manager.
2. Restart your PC.
3. Delete windrvr6.sys from C:\Windows\System32\drivers\
4. Run C:\Xilinx\14.7\ISE_DS\ISE\bin\nt64 in a cmd.exe with admin permissions.

Yay! Now you have a functioning system to work on older Xilinx parts.
