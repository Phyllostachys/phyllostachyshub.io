---
layout: post
title:  "Couches and TVs"
date:   2014-04-03
categories: maths entertainment
---
I was browsing the interwebs and I came across an [article](http://www.cnet.com/news/how-big-a-tv-should-i-buy/) from CNET that was discussing what size TV someone should get depending on how far from the TV they were sitting. I've seen
these sorts of articles before but haven't read one in a while. The thing that I noticed was a multiplier to determine
how big a TV you should get.

The article mentions two recommendations from THX and the Society of Motion Picture and Television Engineers (SMPTE).
THX recommends the TV to fill 40° of one's field of vision whereas SMPTE recommends 30°.

The article list a multiplier of 0.84 for 40° and 0.625 for 30°. I wanted to verify that number so I did the math.

Here is a little diagram I used to reason about the problem:  
![Counch and TV diagram](/images/couch-tv.png)

Since we can shift the TV up so that the bottom lines up with the centerline without changing the results, we'll do
that. This also simplifies the equations a tad. That leaves us with an equation involving _l_ and _w_:  
<center><pre>tan(Θ) = _w_ / _l_                (1)  </pre></center>
which we can modify to find _w_:  
<center><pre>_l_ * tan(Θ) = _w_                (2)  </pre></center>

The mistake that the article made was to finish it there but TVs are not sold by their widths. That is where the
second part of the diagram comes into play. To find the angle of the diagonal, Φ, we need the TV's aspect ratio.
Since we are just looking for the angle, we don't need to match the units of the TV width and height up with the couch
distance and diagonal length.

To find the angle Φ:  
<center><pre>tan(Φ) = _h_ / _w_                (3)  </pre></center>
<center><pre>Φ = arctan(_h_ / _w_)             (4)  </pre></center>

To find the diagonal _d_:  
<center><pre>cos(Φ) = _w_ / _d_                (5)  </pre></center>

and to give us something to equate to equation 2:  
<center><pre>_w_ = _d_ * cos(arctan(_h_/_w_))  (6)  </pre></center>

That leaves us with some algebra to find d:  
<center><pre>_l_ * tan(Θ) = _d_*cos(arctan(_h_ / _w_))  </pre></center>
<center><pre>_d_ = _l_ * tan(Θ) / cos(arctan(_h_ / _w_))  </pre></center>

So the multiplier for a HDTV is 0.963 for 40° and 0.662 for 30°.

In general, to find the right sized TV, use this equation:  
TV diagonal size = distance from couch (in inches) * tan(recommended field of view) / cos(arctan(aspect ratio height / aspect ratio width))