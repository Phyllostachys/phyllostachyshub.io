---
layout: post
title:  "Germophobic Post-Mortem"
date:   2014-05-05
categories: games game-development LD48
---
### [Germophobic][0] Concept
A tower defense that takes place inside the body against germs.

#### What Went Right
We went about coming up with the game concept shortly after the theme was released and it didn't take us too long to arrive at the concept. The division of labor was quickly broken down the next morning when we got started and remained until we finished.

#### What Went Wrong
As the programmer, I wanted it be a browser based game using HTML5/javascript and had picked the [phaser.js][1] library. A short ways into it, I came across problems with how phaser handles collision and how it seems to be better suited to platformer styled games. I then switched to the more basic [pixi.js][2] library and it seemed to go along nicely.

I later had issues trying to get the 2D context of the canvas element so that I could pull pixel data directly for collision detection. I became frustrated with my attempts and reimplemented everything in python with pygame. This happened early Sunday morning and I had everything done later that day but didn't have enough time to get much further (we all had to work Monday so we pretended that we were doing the 48 hr compo).

#### Notes for the future:
- play with the game framework ahead of time (I knew to do this but didn't. :-/ )
- implement all of the art assets seperately so that they can be moved and collision detection can be done more easily

  [0]: http://www.ludumdare.com/compo/ludum-dare-29/?action=preview&uid=34098
  [1]: http://phaser.io/
  [2]: http://www.pixijs.com/
