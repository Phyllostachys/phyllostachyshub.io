---
layout: post
title:  "Catagorizing Interest"
date:   2014-04-02
categories: career interests hobbies
---
Since about 2 years into college, I've been trying to further narrow down my various, abundant interest. I started with just, "Yay computers!" and now feel that I am getting closer to that point where I can say, "I enjoy these things, specifically."

<!--
I recently applied to [Oculus VR](http://www.oculusvr.com/), as an Embedded Systems Engineer. I figured that it would be a great place to be since they have a lot of exciting stuff going on, with the Facebook merger and Michael Abrash leaving Valve (of all places) to be their Chief Scientist. The position is also aligned with my interest (computer graphics, video games, embedded systems) and I fit their job posting. They still sent along a rejection email, likely because I don't have enough experience yet (or, more likely, they have people with equal qualifications that do have the experience).
-->

I have been fairly good about it, even though I don't much like to think of it as 'specialization'.[1] The things that I find that I really like are:
<ul>
<li>computer graphics (with all that delicious math)</li>
<li>programming languages (compilers, interpreters, virtual machines, et al.)</li>
<li>game programming</li>
<li>embedded hw/sw (ARM is very much so winning, which is probably a [Good Thing](http://www.c2.com/cgi/wiki?GoodThing))</li>
</ul>

That is still a little spread out but I feel that these fields are all concerned with performance and tend to get much closer to the bare metal or OS than what enterprise desktop applications do. My other interest have been relegated to the hobby-only domain: FPGAs and Web Applications. Though I have flirted with the idea of being a web app guy, working remotely, in Python or Ruby (or Clojure or ...)

I suppose that I'm often overwhelmed with the stuff that I need (or want) to learn, and the daily amount of time I have to learn it. I feel that it is something like an under-damped system or perhaps a water reservoir where the outlet pipe is too small, requiring more time to drain than what is desired. I then also realize how much free time I wasted while I was in college, or while I was in the military. I suppose that is how it goes though.

[1] - From Robert Heinlein, _"A human being should be able to change a diaper, plan an invasion, butcher a hog, conn a ship, design a building, write a sonnet, balance accounts, build a wall, set a bone, comfort the dying, take orders, give orders, cooperate, act alone, solve equations, analyze a new problem, pitch manure, program a computer, cook a tasty meal, fight efficiently, die gallantly. Specialization is for insects."_ Also, see this [C2 entry](http://c2.com/cgi/wiki?SpecializationIsForInsects).
