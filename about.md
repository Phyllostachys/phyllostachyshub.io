---
layout: page
title: About
---

Hello. My name is Jacob Paul Shaffer. I'm an Embedded Software Engineer in western Pennsylvania, currently at [Matric Limited](https://www.matric.com).<br /><br />
Some of the cool stuff I like to tinker with or work on are embedded programming, graphics programming, and programming languages. Some of that stuff can be found at [GitHub](https://www.github.com/phyllostachys).<br />

[Resume](/assets/jps-resume_20170816.pdf)

<br /><br />
